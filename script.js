
const todoForm = document.querySelector('.form-inline');
const todoInput = document.querySelector('.form-control');
const todoItemsList = document.querySelector('.todo-items');
const editItemsList =document.querySelector('.modal-body');
const editValue=document.querySelector('.edit-form');

let todos = [];


todoForm.addEventListener('submit', function(event) {
  
  event.preventDefault();
  addTodo(todoInput.value); // call addTodo function with input box current value
});

let val;
editItemsList.addEventListener('submit',function(event){
 val=editValue.value;
 console.log(val);
})


function addTodo(item) {
  
  if (item !== '') {
    
    const todo = {
      id: Date.now(),
      name: item,
      completed: false
    };

    
    todos.push(todo);
    addToLocalStorage(todos); 

   
    todoInput.value = '';
  }
}


function renderTodos(todos) {
  
  todoItemsList.innerHTML = '';

  // run through each item inside todos
  todos.forEach(function(item) {
    // check if the item is completed
    const checked = item.completed ? 'checked': null;
    
    // make a <li> element and fill it
    // <li> </li>
    const li = document.createElement('li');
    // <li class="item"> </li>
    li.setAttribute('class', 'item');
    // <li class="item" data-key="20200708"> </li>
    li.setAttribute('data-key', item.id);
    

    li.innerHTML = `
      <input type="checkbox" class="checkbox" ${checked}>
      ${item.name}
      <button class="delete-button">X</button>
      <button  class="edit-todo" data-toggle="modal" data-target="#exampleModalCenter">Edit</button>
    `;
    
    todoItemsList.append(li);
  });

}


function addToLocalStorage(todos) {
 
  localStorage.setItem('todos', JSON.stringify(todos));
  
  renderTodos(todos);
}

// function helps to get everything from local storage
function getFromLocalStorage() {
  const reference = localStorage.getItem('todos');
  // if reference exists
  if (reference) {
    // converts back to array and store it in todos array
    todos = JSON.parse(reference);
    renderTodos(todos);
  }
}

// toggle the value to completed and not completed
function toggle(id) {
  todos.forEach(function(item) {
    // use == not ===, because here types are different. One is number and other is string
    if (item.id == id) {
      // toggle the value
      item.completed = !item.completed;
    }
  });

  addToLocalStorage(todos);
}

// deletes a todo from todos array, then updates localstorage and renders updated list to screen
function deleteTodo(id) {
  // filters out the <li> with the id and updates the todos array
  todos = todos.filter(function(item) {
    // use != not !==, because here types are different. One is number and other is string
    return item.id != id;
  });

  // update the localStorage
  addToLocalStorage(todos);
}

function editTodo(id)
{
  todos = todos.filter(function(item) {
    // use != not !==, because here types are different. One is number and other is string
    return item.id != id;

  });
  console.log(todos);  
}


// initially get everything from localStorage
getFromLocalStorage();

// after that addEventListener <ul> with class=todoItems. Because we need to listen for click event in all delete-button and checkbox
todoItemsList.addEventListener('click', function(event) {
  // check if the event is on checkbox
  if (event.target.type === 'checkbox') {
    // toggle the state
    toggle(event.target.parentElement.getAttribute('data-key'));
  }

  // check if that is a delete-button
  if (event.target.classList.contains('delete-button')) {
    // get id from data-key attribute's value of parent <li> where the delete-button is present
    deleteTodo(event.target.parentElement.getAttribute('data-key'));
  }


  if (event.target.classList.contains('edit-todo')) {
    
   
    editTodo(event.target.parentElement.getAttribute('data-key'));
  }
});

// function editTodo(id)
// {
//   for(var i=0;i<todos.length;i++)
//   {
//   if(todos[i].id != id)
//   {
//     console.log("no");
//   }
//   else
//   {
//     console.log("yes")
//   }
// }
// }

const x = document.querySelectorAll(".edit-todo");
x.forEach(t => t.addEventListener(click, e => {
  console.log(e.target)
}))